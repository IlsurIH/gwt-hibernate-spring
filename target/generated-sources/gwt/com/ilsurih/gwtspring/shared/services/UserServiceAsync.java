package com.ilsurih.gwtspring.shared.services;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.ilsurih.gwtspring.shared.dto.UserDTO;
import java.util.List;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

public interface UserServiceAsync
{

    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.ilsurih.gwtspring.shared.services.UserService
     */
    void findUser( long userId, AsyncCallback<com.ilsurih.gwtspring.shared.dto.UserDTO> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.ilsurih.gwtspring.shared.services.UserService
     */
    void getUsers( AsyncCallback<java.util.List<com.ilsurih.gwtspring.shared.dto.UserDTO>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.ilsurih.gwtspring.shared.services.UserService
     */
    void saveUser( long userId, java.lang.String name, java.lang.String password, AsyncCallback<Void> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.ilsurih.gwtspring.shared.services.UserService
     */
    void updateUser( long userId, java.lang.String name, java.lang.String password, AsyncCallback<Void> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.ilsurih.gwtspring.shared.services.UserService
     */
    void saveOrUpdateUser( long userId, java.lang.String name, java.lang.String password, AsyncCallback<Void> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.ilsurih.gwtspring.shared.services.UserService
     */
    void deleteUser( long userId, AsyncCallback<Void> callback );


    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static UserServiceAsync instance;

        public static final UserServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (UserServiceAsync) GWT.create( UserService.class );
                ServiceDefTarget target = (ServiceDefTarget) instance;
                target.setServiceEntryPoint( GWT.getModuleBaseURL() + "springGwtServices/userService" );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instanciated
        }
    }
}
