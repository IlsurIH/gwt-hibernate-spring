package com.ilsurih.gwtspring.shared.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.ilsurih.gwtspring.shared.dto.UserDTO;

import java.util.List;

@RemoteServiceRelativePath("springGwtServices/userService")
public interface UserService extends RemoteService {
	public UserDTO findUser(long userId);
    public List<UserDTO> getUsers();
	public void saveUser(long userId, String name, String password) throws Exception;
	public void updateUser(long userId, String name, String password) throws Exception;
	public void saveOrUpdateUser(long userId, String name, String password) throws Exception;
	public void deleteUser(long userId) throws Exception;
	
}
