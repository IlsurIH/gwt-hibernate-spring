package com.ilsurih.gwtspring.shared.dto;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class UserDTO implements java.io.Serializable {
	
	private static final long serialVersionUID = 7440297955003302414L;

	@Id
    @GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name="id")
	private long userId;
	
	@Column(name="login", nullable = false)
	private String userLogin;
	
	@Column(name="password", nullable = false)
	private String userPassword;

		
	public UserDTO() {
	}

	public UserDTO(int userId) {
		this.userId = userId;
	}

	public UserDTO(long userId, String userLogin, String userPassword) {
		this.userId = userId;
		this.userLogin = userLogin;
		this.userPassword = userPassword;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
}