package com.ilsurih.gwtspring.server.services;

import com.ilsurih.gwtspring.server.dao.UserDAO;
import com.ilsurih.gwtspring.shared.dto.UserDTO;
import com.ilsurih.gwtspring.shared.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @PostConstruct
    public void init() throws Exception {
    }

    @PreDestroy
    public void destroy() {
    }

    public UserDTO findUser(long userId) {

        return userDAO.findById(userId);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveUser(long userId, String login, String password) throws Exception {

        UserDTO userDTO = userDAO.findById(userId);

        if (userDTO == null) {
            userDTO = new UserDTO(userId, login, password);
            userDAO.persist(userDTO);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void updateUser(long userId, String login, String password) throws Exception {

        UserDTO userDTO = userDAO.findById(userId);

        if (userDTO != null) {
            userDTO.setUserLogin(login);
            userDTO.setUserPassword(password);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteUser(long userId) throws Exception {

        UserDTO userDTO = userDAO.findById(userId);

        if (userDTO != null)
            userDAO.remove(userDTO);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public List<UserDTO> getUsers() {
        return userDAO.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveOrUpdateUser(long userId, String login, String password) throws Exception {

        UserDTO userDTO = new UserDTO(userId, login, password);

        userDAO.merge(userDTO);

    }

}
