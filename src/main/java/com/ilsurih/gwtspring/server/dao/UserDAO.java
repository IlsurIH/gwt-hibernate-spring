package com.ilsurih.gwtspring.server.dao;
/**
 * Created by ilsurih on 17.07.14.
 */

import com.ilsurih.gwtspring.shared.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;


@Repository("userDAO")
public class UserDAO extends JpaDAO<Long, UserDTO> {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @PostConstruct
    public void init() {
        super.setEntityManagerFactory(entityManagerFactory);
    }

}
