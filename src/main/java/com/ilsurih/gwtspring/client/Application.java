package com.ilsurih.gwtspring.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.xml.client.Element;
import com.ilsurih.gwtspring.shared.dto.UserDTO;
import com.ilsurih.gwtspring.shared.services.UserServiceAsync;

import java.util.List;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Application
        implements EntryPoint {

    //<-- LOGIN -->
    private RootPanel loginContainer;
    private PasswordTextBox passwordInput;
    private ListBox userListBox;
    private Button loginButton;
    private Button dialogCloseButton;
    private DialogBox dialogBox;
    private InlineHyperlink showRegistration;
//<!-- LOGIN -->

    //<-- REGISTRATION -->
    private RootPanel registrationContainer;
    private TextBox regLogin;
    private PasswordTextBox regPassword1;
    private PasswordTextBox regPassword2;
    private Button registrationButton;
//<!-- REGISTRATION -->

    private static List<UserDTO> users;
    private final UserServiceAsync userService = UserServiceAsync.Util.getInstance();

    public Application() {
    }

    private class registrationHandler implements ClickHandler, KeyUpHandler {

        @Override
        public void onClick(ClickEvent event) {
            tryRegistr();
        }

        @Override
        public void onKeyUp(KeyUpEvent event) {
            if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER)
                tryRegistr();
        }
    }

    private class loginHandler implements ClickHandler, KeyUpHandler {

        @Override
        public void onClick(ClickEvent event) {
            tryLogin();
        }

        @Override
        public void onKeyUp(KeyUpEvent event) {
            if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER)
                tryLogin();
        }
    }

    private void tryRegistr() {
        String login = regLogin.getText();
        String password1 = regPassword1.getText();
        String password2 = regPassword2.getText();

        dialogBox = new DialogBox();
        final VerticalPanel panel = new VerticalPanel();
        final String[] dialogText = {""};

        class Async implements AsyncCallback<Void>{

            @Override
            public void onFailure(Throwable caught) {
                dialogText[0] = "Database error";
                setupDialog();
            }

            @Override
            public void onSuccess(Void result) {
                dialogText[0] = "Success!";
                setupDialog();
                setupLoginContainer();
            }

            private void setupDialog() {
                panel.add(new Label(dialogText[0]));

                (dialogCloseButton = new Button("close")).addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        dialogBox.hide();
                    }
                });
                panel.add(dialogCloseButton);
                dialogBox.add(panel);
                dialogBox.center();
                dialogCloseButton.setFocus(true);

            }
        }
        Async callback = new Async();

        if (!password1.equals(password2)) {
            dialogText[0] = "Passwords are not the same!";
            callback.setupDialog();
        } else {
            userService.saveUser(generateID(), login, password1, callback);
        }
    }

    private long generateID() {
        boolean end = false;
        long i = -1;
        while (!end) {
            i++;
            end = true;
            for (UserDTO user : users) {
                if (user.getUserId() == i)
                    end = false;
            }
        }

        return i;
    }

    private void tryLogin() {
        String password = passwordInput.getText();
        String login = userListBox.getValue(userListBox.getSelectedIndex());

        dialogBox = new DialogBox();
        VerticalPanel panel = new VerticalPanel();
        String dialogText = null;
        for (UserDTO u : users) {
            if (u.getUserPassword().equals(password) && u.getUserLogin().equals(login)) {
                dialogText = "Correct!";
                break;
            }
        }
        if (dialogText == null)
            dialogText = "False!";
        panel.add(new Label(dialogText + login+ " "+ password));

        (dialogCloseButton = new Button("close")).addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                dialogBox.hide();
            }
        });
        panel.add(dialogCloseButton);
        dialogBox.add(panel);
        dialogBox.center();
        dialogCloseButton.setFocus(true);

    }

    @Override
    public void onModuleLoad() {
        setupLoginContainer();
        setupRegistrationContainer();
    }

    private void setupRegistrationContainer() {

        registrationContainer = RootPanel.get("registrationContainer");

        regLogin = new TextBox();
        regPassword1 = new PasswordTextBox();
        regPassword2 = new PasswordTextBox();
        registrationButton = new Button("Sign up");

        VerticalPanel panel = new VerticalPanel();
        panel.add(new Label("Login:"));
        panel.add(regLogin);
        panel.add(new Label("Password:"));
        panel.add(regPassword1);
        panel.add(new Label("Confirm, please:"));
        panel.add(regPassword2);
        panel.add(registrationButton);

        registrationButton.addClickHandler(new registrationHandler());
        registrationContainer.add(panel);
    }

    private void setupLoginContainer() {

        if (userListBox == null) {
            showRegistration = new InlineHyperlink("I don't have account", "");
            showRegistration.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    registrationContainer.setVisible(true);
                    showRegistration.setVisible(false);
                }
            });
            loginContainer = RootPanel.get("loginContainer");
            passwordInput = PasswordTextBox.wrap(DOM.getElementById("passwordInput"));
            userListBox = ListBox.wrap(DOM.getElementById("listBox"));
            loginContainer.add(userListBox);
            loginContainer.add(passwordInput);
            loginContainer.add(loginButton = Button.wrap(DOM.getElementById("loginButton")));
            loginContainer.add(showRegistration);
            passwordInput.addKeyUpHandler(new loginHandler());
            loginButton.addClickHandler(new loginHandler());
        } else {
            userListBox.clear();
        }
        addUsers(userListBox);
    }

    private void addUsers(final ListBox lb) {

        AsyncCallback<List<UserDTO>> callback = new AsyncCallback<List<UserDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(List<UserDTO> userDTOs) {
                users = userDTOs;
                for (UserDTO userDTO : userDTOs) {
                    lb.addItem(userDTO.getUserLogin());
                }
            }
        };
        userService.getUsers(callback);
    }

    public static void main(String[] args) {

    }
}
